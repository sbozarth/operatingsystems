/************************************/
/* Authored by Daniel Scott Bozarth */
/************************************/

/* base region for data starts at 2, so when you acess (cluster-2)*clusterSize
/* each cluster serves only one purpose
/* if you look in a cluster and the filename isnt their, check FAT to see if it points to any other cluster
/* you have to care about short in the filename.
/* when you print a cluster print one charecter at a time until you reach the file size.
/* space pad everything
*/

#define VERSION_NUM 0.0

#include "fs.hpp"
#include <getopt.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>
#include <cstdlib>
#include <string.h>
#include <sys/stat.h>
#include <ctype.h>

#define EOC 0xFFF8

#define BUFFER_SIZE 32
char delimiters[] = " \n\r";
uint8_t isDebug = 0;
uint8_t testmmap = 0;
uint8_t testBootSector = 0;
uint8_t testDirectoryEntry = 0;
uint8_t testDirectoryCluster = 0;

union Data{
    char c;
    unsigned char b;
    int16_t s;
    uint16_t w;
    int32_t i;
    uint32_t u;
};

/*
/* 0x00 8 Short File Name, 0x00 available, 0x05, first charecter is 0xE5, 0x2E . or .., 0xE5 Entry has been deleted
/* 0x08 3 Short File Extention
/* 0x0B 1 File Attributes, mask 0x01 read only, mask 0x02 hidden, mask 0x04 system, mask 0x08 volume label, mask 0x10 subdirectory, mask 0x20 archive, mask 0x40 device, mask 0x80 reserved
/* 0x0C 1 0x0
/* 0x0D 1 Create Time Millis, from 0 to 199 ms
/* 0x0E 2 Create Time, bits 15-11 hours, bits 10-5 minutes, bits 4-0 seconds/2
/* 0x10 2 Create Date, bits 15-9 year (0 = 1980), bits 8-5 month, bits 4-0 day
/* 0x12 2 Last Access Date, bits 15-9 year (0 = 1980), bits 8-5 month, bits 4-0 day
/* 0x14 2 Extended Attributes, mask 0x0001 Owner elete/rename/attribute requires permission, mask 0x0002 owner execute requires permission, mask 0x0004 owner right modify requires permission, mask 0x0008 owner read/copy requires permission, group, world
/* 0x16 2 Last Modified Time, bits 15-11 hours, bits 10-5 minutes, bits 4-0 seconds/2
/* 0x18 2 Last Modified Date,  bits 15-9 year (0 = 1980), bits 8-5 month, bits 4-0 day
/* 0x1A 2 First location in file, start of the file in clusters
/* 0x1C 4 Size of File, file sisze in bytes
*/

struct DirectoryEntry{
    char shortFileName[9];
    char shortExtension[4];
    uint8_t fileAttributes;
    uint8_t createMillis;
    uint16_t createTime;
    uint16_t createDate;
    uint16_t lastAccessDate;
    uint16_t extendedArtibutes;
    uint16_t lastModifiedTime;
    uint16_t lastModifiedDate;
    uint16_t firstLocation;
    uint32_t sizeOfFile;
};

struct TranslatedDirectoryEntry{

};

char *trimwhitespace(char *str)
{
  char *end;

  // Trim leading space
  while(isspace((unsigned char)*str)) str++;

  if(*str == 0)  // All spaces?
    return str;

  // Trim trailing space
  end = str + strlen(str) - 1;
  while(end > str && isspace((unsigned char)*end)) end--;

  // Write new null terminator character
  end[1] = '\0';

  return str;
}

uint8_t attributeCheckReadOnly(uint8_t attribute){
    return attribute&0x01;
}
uint8_t attributeCheckHidden(uint8_t attribute){
    return attribute&0x02;
}
uint8_t attributeCheckSystem(uint8_t attribute){
    return attribute&0x04;
}
uint8_t attributeCheckVolumeLabel(uint8_t attribute){
    return attribute&0x08;
}
uint8_t attributeCheckSubdirectory(uint8_t attribute){
    return attribute&0x10;
}
uint8_t attributeCheckArchive(uint8_t attribute){
    return attribute&0x20;
}
uint8_t attributeCheckDevice(uint8_t attribute){
    return attribute&0x40;
}

uint16_t translateYear(uint16_t FATdate){
    return ((FATdate>>9)&0x7f)+1980;
}
uint8_t translateMonth(uint16_t FATdate){
    return (FATdate>>5)&0xf;
}

uint8_t translateDay(uint16_t FATdate){
    return FATdate&0x1F;
}

uint8_t translateHour(uint16_t FATtime){
    return (FATtime>>11)&0x1F;
}
uint8_t translateMinutes(uint16_t FATtime){
    return (FATtime>>5)&0x3F;
}

uint8_t translateSeconds(uint16_t FATtime, uint8_t FATmillis){
    return ((FATtime<<1)+(FATmillis/100))&0x3F;
}
uint8_t translateMillis(uint16_t FATmillis){
    return (FATmillis%100)*10;
}



int populateDirectoryStruct(struct DirectoryEntry *currentEntry, char * directoryOffset){ /* returns whether empty directory or not */
    #define SHORT_FILENAME_SIZE 8
    #define SHORT_FILENAME_OFFSET 0x0
    currentEntry->shortFileName[8] = '\0';

    memcpy(currentEntry->shortFileName,(directoryOffset+SHORT_FILENAME_OFFSET), SHORT_FILENAME_SIZE);
    
    #define SHORT_EXTENSION_SIZE 3
    #define SHORT_EXTENSION_OFFSET 0x8
    currentEntry->shortExtension[3] = '\0';
    memcpy(currentEntry->shortExtension,(directoryOffset+SHORT_EXTENSION_OFFSET), SHORT_EXTENSION_SIZE);

    #define DIRECTORY_FILE_ATTRIBUTES_OFFSET 0x0B
    currentEntry->fileAttributes = *(uint8_t *)(directoryOffset+DIRECTORY_FILE_ATTRIBUTES_OFFSET);

    #define DIRECTORY_CREATE_MILLIS_OFFSET 0x0D
    currentEntry->createMillis = *(uint8_t *)(directoryOffset+DIRECTORY_CREATE_MILLIS_OFFSET);

    #define DIRECTORY_CREATE_TIME_OFFSET 0x0E
    currentEntry->createTime = *(uint16_t *)(directoryOffset+DIRECTORY_CREATE_TIME_OFFSET);

    #define DIRECTORY_CREATE_DATE_OFFSET 0x10
    currentEntry->createDate = *(uint16_t *)(directoryOffset+DIRECTORY_CREATE_DATE_OFFSET);

    #define DIRECTORY_LAST_ACCESS_DATE_OFFSET 0x12
    currentEntry->lastAccessDate = *(uint16_t *)(directoryOffset+DIRECTORY_LAST_ACCESS_DATE_OFFSET);

    #define DIRECTORY_EXTENDED_ATTRIBUTES_OFFSET 0x14
    currentEntry->extendedArtibutes = *(uint16_t *)(directoryOffset+DIRECTORY_EXTENDED_ATTRIBUTES_OFFSET);

    /* Where is last modified millis? */

    #define DIRECTORY_LAST_MODIFIED_TIME_OFFSET 0x16
    currentEntry->lastModifiedTime = *(uint16_t *)(directoryOffset+DIRECTORY_LAST_MODIFIED_TIME_OFFSET);

    #define DIRECTORY_LAST_MODIFIED_DATE_OFFSET 0x18
    currentEntry->lastModifiedDate = *(uint16_t *)(directoryOffset+DIRECTORY_LAST_MODIFIED_DATE_OFFSET);

    #define DIRECTORY_START_CLUSTER_OFFSET 0x1A
    currentEntry->firstLocation = *(uint16_t *)(directoryOffset+DIRECTORY_START_CLUSTER_OFFSET);

    #define DIRECTORY_FILESIZE_SIZE 4
    #define DIRECTORY_FILSIZE_OFFSET 0x1C
    currentEntry->sizeOfFile = *(uint32_t *)(directoryOffset+DIRECTORY_FILSIZE_OFFSET);
    if(currentEntry->sizeOfFile == 0 && !(attributeCheckVolumeLabel(currentEntry->fileAttributes)||attributeCheckSubdirectory(currentEntry->fileAttributes))){ /* file size 0, not volume label or subdirectory */
        return 1;
    }
    return 0;
}
# if 0
int travelDirectory(char * directoryName, struct DirectoryEntry *currentEntry, char * directoryOffset, uint16_t maxRootDirectoryEntries){
    /* when we are in a subdirectory, the size tells us how far we can go */
    return /* start cluster of file */;
}
#endif

uint8_t findFileStart = 0;
uint8_t printFileContents = 0;
uint8_t isValidDirectory = 0;
uint8_t isEmptyDirectory = 0;

void recursiveCheck(struct DirectoryEntry *currentEntry, char * fileNameBuffer, char* delimiters, char* subDelimiters, char* dataRegionOffset, char* FATregionOffset, int clusterSize){
    if(attributeCheckSubdirectory(currentEntry->fileAttributes)){ /* we have a subdir go recursive */
        char * token = strsep(&fileNameBuffer, delimiters);
        char * currentSearchLong;
        char * currentSearchShort;
        char * currentSearchString = (char * )malloc(sizeof(char)*(13));
        currentSearchLong = strsep(&token, subDelimiters);
        currentSearchShort = token;
        sprintf(currentSearchString, "%-8.8s.%-3.3s", currentSearchLong, currentSearchShort==NULL?"   ":currentSearchShort);
        int currentLocation = 0;
        int currentCluster = currentEntry->firstLocation;
        uint8_t stayInLoop = 1;
        while(stayInLoop){
            if(currentLocation*32 < clusterSize){
                populateDirectoryStruct(currentEntry, dataRegionOffset+clusterSize*(currentCluster-2)+currentLocation*32);
                ++currentLocation;
                char * currentCompareString = (char * )malloc(sizeof(char)*(13));
                sprintf(currentCompareString, "%-8.8s.%-3.3s", currentEntry->shortFileName, currentEntry->shortExtension);
                if(currentCompareString[0] == 0x05){
                    currentCompareString[0] = 0xE5;
                }
                if(!strncmp(currentCompareString, currentSearchString, 12)){
                    stayInLoop = 0;
                    isValidDirectory = 1;
                    isEmptyDirectory = 0;
                    recursiveCheck(currentEntry, fileNameBuffer, delimiters, subDelimiters, dataRegionOffset, FATregionOffset, clusterSize);
                }
                free(currentCompareString);
            }
            else{ /* not in this cluster, look in next */
                uint16_t nextClusterEntry = *(uint16_t *)(FATregionOffset + currentCluster*2);
                if(nextClusterEntry < EOC &&(nextClusterEntry > 1)){
                    currentCluster = nextClusterEntry;
                }
                else{
                    stayInLoop = 0;
                    isValidDirectory = 0;
                }
            }
        }
        free(currentSearchString);
    }
}

uint8_t testNumEntries = 0;
uint8_t testSpaceUsage = 0;
uint8_t testLargestFile = 0;
uint8_t testCookie = 0;
uint8_t testNumDirLevels = 0;
uint8_t testOldestFile = 0;

int numFiles = 0;
int numDir = 0;
int maxDirDepth = 1;
int largestFileSize = 0;
uint64_t oldestFileTime = -1;
char * largestFileString;
char * oldestFileString;
char * cookieFile;
int cookieCluster = 0;
uint32_t numberUsedClusters = 0;
uint32_t sizeOfFiles = 0;

void traverseDirectory(struct DirectoryEntry *previousEntry, char* dataRegionOffset, char* FATregionOffset, int clusterSize, char * previousLocation, int dirDepth){
    //sprintf(currentSearchString, "%-8.8s.%-3.3s", currentSearchLong, currentSearchShort==NULL?"   ":currentSearchShort);
    /* Need to make one that cats previousLocation to new location correctly now */
    int currentLocation = 0;
    int currentCluster = previousEntry->firstLocation;
    uint8_t stayInLoop = 1;
    while(stayInLoop){
        struct DirectoryEntry currentEntry;
        if(currentLocation*32 < clusterSize){
            populateDirectoryStruct(&currentEntry, dataRegionOffset+clusterSize*(currentCluster-2)+currentLocation*32);
            if (((unsigned char)currentEntry.shortFileName[0]!=0xE5)&&((unsigned char)currentEntry.shortFileName[0]!=0x2E)){
                sizeOfFiles += currentEntry.sizeOfFile;
            }
            ++currentLocation;
            int maxStringSize = 13+dirDepth*9;
            char * currentCompareString = (char * )malloc(sizeof(char)*(maxStringSize));
            strcpy(currentCompareString, previousLocation);
            strcat(currentCompareString, "/");
            strcat(currentCompareString, currentEntry.shortFileName);
            trimwhitespace(currentCompareString);
            if(currentCompareString[dirDepth*9] == 0x05){ /* should replace anywhere */
                currentCompareString[dirDepth*9] = 0xE5;
            }
            
            if(attributeCheckArchive(currentEntry.fileAttributes)&&((unsigned char)currentEntry.shortFileName[0]!=0xE5)){
                ++numFiles;
                if(dirDepth > maxDirDepth){
                    maxDirDepth = dirDepth;
                }
                strcat(currentCompareString, ".");
                strcat(currentCompareString, currentEntry.shortExtension);
                trimwhitespace(currentCompareString);
            }
            if(attributeCheckArchive(currentEntry.fileAttributes)&&((unsigned char)currentEntry.shortFileName[0]!=0xE5)){
                if(currentEntry.sizeOfFile > largestFileSize&&((unsigned char)currentEntry.shortFileName[0]!=0xE5)&&((unsigned char)currentEntry.shortFileName[0]!=0x2E)){
                    largestFileSize = currentEntry.sizeOfFile;
                    largestFileString = (char * )malloc(sizeof(char)*(maxStringSize));
                    strcpy(largestFileString, currentCompareString);
                }
                uint64_t pseudoTimestamp = (currentEntry.createDate << 24) + (currentEntry.createTime << 8) + currentEntry.createMillis;
                if( pseudoTimestamp < oldestFileTime&&((unsigned char)currentEntry.shortFileName[0]!=0xE5)&&((unsigned char)currentEntry.shortFileName[0]!=0x2E)){
                    oldestFileTime = pseudoTimestamp;
                    oldestFileString = (char * )malloc(sizeof(char)*(maxStringSize));
                    strcpy(oldestFileString, currentCompareString);
                }

                int currentClusterEntry = currentEntry.firstLocation;
                int sizeOfFile = currentEntry.sizeOfFile;
                uint8_t keepGoing = 1;
                uint8_t correctChars = 0;
                int size = 0;
                char cookie[15];
                strcpy(cookie, "COS 421 cookie");
                //printf("%s\n", currentCompareString);
                if(currentClusterEntry < EOC &&(currentClusterEntry > 1)){
                    //printf("New Cluster: %d\n", currentClusterEntry);
                }
                while(keepGoing){
                    int currentByte = 0;
                    while( size < sizeOfFile && currentByte < clusterSize){
                        char charByte = *(char *)(dataRegionOffset + (currentClusterEntry-2)*clusterSize+currentByte);
                        if(charByte == cookie[correctChars]){ /* not triggering */
                            ++correctChars;
                        }
                        else{
                            correctChars = 0;
                        }
                        if (correctChars == 14){
                            cookieCluster = currentEntry.firstLocation; /* not cluster cookie is in, but cluster for start of file cookie is in */
                            cookieFile = (char * )malloc(sizeof(char)*(maxStringSize));
                            strcpy(cookieFile, currentCompareString);
                            keepGoing = 0;
                            size = sizeOfFile;
                        }
                        ++size;
                        ++currentByte;
                    }
                    currentClusterEntry = *(uint16_t *)(FATregionOffset + currentClusterEntry*2);
                    if(currentClusterEntry < EOC &&(currentClusterEntry > 1)){
                        //printf("%s\n", currentCompareString);
                        //printf("New Cluster: %d\n", currentClusterEntry);
                    }
                    else{
                        keepGoing = 0;
                    }
                }
            }
            if(attributeCheckSubdirectory(currentEntry.fileAttributes)&&(currentEntry.sizeOfFile==0)){
                if((strncmp(currentEntry.shortFileName, ".",1)!=0)&&((unsigned char)currentEntry.shortFileName[0]!=0xE5)){
                    //printf("%s\n", currentCompareString);
                    //printf("New Cluster: %d\n", currentEntry.firstLocation);
                    ++numDir;
                    traverseDirectory(&currentEntry, dataRegionOffset, FATregionOffset, clusterSize, currentCompareString, dirDepth +1);
                }
            }
            free(currentCompareString);
        }
        else{ /* not in this cluster, look in next */
            uint16_t nextClusterEntry = *(uint16_t *)(FATregionOffset + currentCluster*2);
            if(nextClusterEntry < EOC &&(nextClusterEntry > 1)){
                currentCluster = nextClusterEntry;
            }
            else{
                stayInLoop = 0;
                isValidDirectory = 0;
            }
        }
    }
    //free(currentSearchString);
}

int main(int argc, char * argv[]){
    int opt;
    char* imageName;
    int directoryLocation = 1;
    int clusterDirectoryLocation = -1;
    char* fileNameBuffer;

    const char* short_opt = "i:m:b:d:c:n:f:e:s:l:k:r:o:a:w";
    struct option   long_opt[] =
    {
        {"image", required_argument, NULL, 'i'},
        {"test-mmap", no_argument, NULL, 'm'},
        {"test-boot-sector", no_argument, NULL, 'b'},
        {"test-directory-entry", required_argument, NULL, 'd'},
        {"test-file-clusters", required_argument, NULL, 'c'},
        {"invalid-image", no_argument, NULL, 'x'},
        {"test-file-name", required_argument, NULL, 'n'},
        {"test-file-contents", required_argument, NULL, 'f'},
        {"test-num-entries", no_argument, NULL, 'e'},
        {"test-space-usage", no_argument, NULL, 's'},
        {"test-largest-file", no_argument, NULL, 'l'},
        {"test-cookie", no_argument, NULL, 'k'},
        {"test-num-dir-levels", no_argument, NULL, 'r'},
        {"test-oldest-file", no_argument, NULL, 'o'},
        {"output-fs-data", no_argument, NULL, 'a'},
        {"write-fs-data", no_argument, NULL, 'w'},
        {NULL,            0,                 NULL, 0  }
    };
    while ((opt = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1){
        switch (opt){
            case 0:
                break;
            case 'i':
                imageName = optarg;
                break;
            case 'm':
                testmmap = 1;
                break;
            case 'b':
                testBootSector = 1;
                break;
            case 'd':
                directoryLocation = atoi(optarg);
                testDirectoryEntry = 1;
                break;
            case 'c':
                clusterDirectoryLocation = atoi(optarg);
                testDirectoryCluster = 1;
                break;
            case 'x':
                isValidDirectory = 0;
                break;
            case 'n':
                testDirectoryEntry = 1;
                findFileStart = 1;
                fileNameBuffer = optarg;
                break;
            case 'f':
                findFileStart = 1;
                printFileContents = 1;
                fileNameBuffer = optarg;
                break;
            case 'e':
                testNumEntries = 1;
                break;
            case 's':
                testSpaceUsage = 1;
                break;
            case 'l':
                testLargestFile = 1;
                break;
            case 'k':
                testCookie = 1;
                break;
            case 'r':
                testNumDirLevels = 1;
                break;
            case 'o':
                testOldestFile = 1;
                break;
            case 'a':
                testNumEntries = 1;
                testSpaceUsage = 1;
                testLargestFile = 1;
                testCookie = 1;
                testNumDirLevels = 1;
                testOldestFile = 1;
                break;
            case 'w':
                break;
            default:
                break;
        }
    }
    int fileDescriptor;
    size_t pageSize;
    char * region;
    struct stat st;
    if (imageName == NULL){
        printf("Error: No Image Found\n");
        return -1;
    }
    else{
        fileDescriptor = open(imageName, O_RDWR ); /* We will need to be able to add stuff eventualy */
        if (stat(imageName, &st) != 0){
            return -1;
        }
        pageSize = st.st_size;
        /* for some reason region has no contents */
        region = (char *) mmap(NULL, pageSize, PROT_READ|PROT_WRITE, MAP_SHARED,fileDescriptor, 0);
    }
    #define OEM_OFFSET 0x3
    #define OEM_SIZE 8
    char OEM[9];
    OEM[8] = '\0';
    strncpy(OEM,(region+OEM_OFFSET), OEM_SIZE);
    #define BPS_OFFSET 0xB
    uint16_t bytesPerSector = *(uint16_t *)(region+BPS_OFFSET);
    #define SPC_OFFSET 0xD
    uint8_t sectorsPerCluster = *(uint8_t *)(region+SPC_OFFSET);
    #define RLS_OFFSET 0xE
    uint16_t reservedSectors = *(uint16_t *)(region+RLS_OFFSET);
    #define NFAT_OFFSET 0x10
    uint8_t numFATs = *(uint8_t *)(region+NFAT_OFFSET);
    #define MFRD_OFFSET 0x11
    uint16_t maxRootDirectoryEntries = *(uint16_t *)(region+MFRD_OFFSET);
    #define TLS_OFFSET 0x13
    #define BIG_TLS_OFFSET 0x20
    uint32_t totalSectors = *(uint16_t *)(region+TLS_OFFSET);
    if (totalSectors == 0){
         totalSectors = *(uint32_t *)(region+BIG_TLS_OFFSET);
    }
    #define MD_OFFSET 0x15
    uint8_t mediaDescriptor = *(uint8_t *)(region+MD_OFFSET);
    #define LSPFAT_OFFSET 0x16
    uint16_t sectorsPerFAT = *(uint16_t *)(region+LSPFAT_OFFSET);
    if (testBootSector){
        printf("OEM: %s\n", OEM);
        printf("Bytes per sector: %i\n", bytesPerSector);
        printf("Sectors per cluster: %i\n", sectorsPerCluster);
        printf("Reserved sectors: %i\n", reservedSectors);
        printf("Num FATs: %i\n", numFATs);
        printf("Max root directory entries: %i\n", maxRootDirectoryEntries);
        printf("Num logical sectors: %i\n", totalSectors);
        printf("Media Descriptor: %x\n", mediaDescriptor);
        printf("Sectors per FAT: %i\n", sectorsPerFAT);
    }
    if (testmmap){
        char buffer[BUFFER_SIZE];
        char* sizeString;
        char size;
        char* addressString;
        unsigned int address;
        Data data;
        int temp;
        while (fgets(buffer, BUFFER_SIZE, stdin)!=NULL){ /* read in from stdin */
            sizeString = strtok(buffer,delimiters);
            size = *sizeString;
            addressString = strtok(NULL, delimiters);
            address = strtol(addressString, NULL, 10);
            switch (size)
            {
                case 'c':
                    data.c = *(region+address);
                    printf("%c\n", data.c);
                    break;
                case 'b':
                    data.b = *(uint8_t *)(region+address);
                    printf("%x\n", data.b);
                    break;
                case 's':
                    data.s = *(int16_t *)(region+address);
                    printf("%i\n", data.s);
                    break;
                case 'w':
                    data.w = *(uint16_t *)(region+address);
                    printf("%x\n", data.w);
                    break;
                case 'i':
                    data.i = *(int32_t *)(region+address);
                    printf("%i\n", data.i);
                    break;
                case 'u':
                    data.u = *(uint32_t *)(region+address);
                    printf("%x\n", data.u);
                    break;
                default:
                    break;
            }
            /* some sort of structure for the different address types */
            /* c - char                        */
            /* b - unsigned hexidecimal byte   */
            /* s - 2 byte decimal              */
            /* w - unsigned 2 byte hexadecimal */
            /* i - 4 byte decimal              */
            /* u - unsigned 4 byte hexadecimal */
            /* cast void * to the right type */
        }
    }
    char* rootDirectoryOffset = region + reservedSectors*bytesPerSector + numFATs*sectorsPerFAT*bytesPerSector;
    if ((unsigned long int)((rootDirectoryOffset + maxRootDirectoryEntries*32)-region) <= st.st_size){
        //printf("%u <= %u\n", ((rootDirectoryOffset + maxRootDirectoryEntries*32)-region), st.st_size);
    }
    else{
        //printf("Invalid Root");
        int unmap_result = munmap(region, pageSize);
        close(fileDescriptor);
        return 0;
    }
    DirectoryEntry currentEntry;
    if (numFATs*sectorsPerFAT >= directoryLocation){
        isValidDirectory = 1;
        isEmptyDirectory = populateDirectoryStruct(&currentEntry, rootDirectoryOffset+directoryLocation*32);
    }
    else{
        printf("Out of Bounds\n");
    }
    char* FATregionOffset = region + reservedSectors*bytesPerSector;
    
    if(testDirectoryCluster){ //&&  isValidDirectory){
        if((clusterDirectoryLocation == 0)||(clusterDirectoryLocation == 1)){
            printf("EOF\n");
        }
        else{
            printf("%d",clusterDirectoryLocation);
            uint16_t currentClusterEntry = *(uint16_t *)(FATregionOffset + clusterDirectoryLocation*2);
            while(currentClusterEntry < EOC &&(currentClusterEntry > 1)){
                printf(" -> %d", currentClusterEntry);
                currentClusterEntry = *(uint16_t *)(FATregionOffset + currentClusterEntry*2); /* size is 16 bits */
            }
            if(clusterDirectoryLocation < EOC){
                printf(" -> EOF\n");
            }
        }
    }
    char delimiters[] = "/\\";
    char subDelimiters[] = ".";
    char * DataRegionOffset = region + reservedSectors*bytesPerSector + numFATs*sectorsPerFAT*bytesPerSector + maxRootDirectoryEntries*32;
    if(findFileStart){
        strsep(&fileNameBuffer, delimiters);
        char * token = strsep(&fileNameBuffer, delimiters);
        char * currentSearchLong;
        char * currentSearchShort;
        char * currentSearchString = (char * )malloc(sizeof(char)*(13));
        currentSearchLong = strsep(&token, subDelimiters);
        currentSearchShort = token;
        sprintf(currentSearchString, "%-8.8s.%-3.3s", currentSearchLong, currentSearchShort==NULL?"   ":currentSearchShort);
        int currentLocation = 0;
        uint8_t stayInLoop = 1;
        while(stayInLoop){
            if(currentLocation < maxRootDirectoryEntries){
                populateDirectoryStruct(&currentEntry, rootDirectoryOffset+currentLocation*32);
                ++currentLocation;
                char * currentCompareString = (char * )malloc(sizeof(char)*(13));
                sprintf(currentCompareString, "%-8.8s.%-3.3s", currentEntry.shortFileName, currentEntry.shortExtension);
                if(currentCompareString[0] == 0x05){
                    currentCompareString[0] = 0xE5;
                }
                if(!strncmp(currentCompareString, currentSearchString, 12)){
                    stayInLoop = 0;
                    isValidDirectory = 1;
                    isEmptyDirectory = 0;
                }
                free(currentCompareString);
            }
            else{
                stayInLoop = 0;
                isValidDirectory = 0;
            }
        }
        if(isValidDirectory){
            recursiveCheck(&currentEntry, fileNameBuffer, delimiters, subDelimiters, DataRegionOffset, FATregionOffset, bytesPerSector*sectorsPerCluster);
        }
        free(currentSearchString);
    }
    if((testDirectoryEntry) && isValidDirectory){ 
        if(isEmptyDirectory){
            printf("Empty entry\n");
        }
        else{
            uint8_t printedFirst = 0;
            switch((unsigned char)currentEntry.shortFileName[0]){
                case 0xE5:
                    printf("Previously erased entry\nName: ?");
                    printedFirst = 1;
                    break;
                case 0x05:
                    printf("Name: %c", 0xE5);
                    printedFirst = 1;
                    break;
                case 0x2E:
                    break;
                default:
                    break;
            }
            #if 0
            for(int i = 0; i < 8; ++i){
                printf("%c", currentEntry.shortFileName[i]);
            }
            #endif
            if (printedFirst){
                printf("%s", &currentEntry.shortFileName[1]);
            }
            else{
                printf("Name: ");
                for(int i = 0; i < 8; ++i){
                    printf("%c", currentEntry.shortFileName[i]);
                }
            }
            #if 0
            if(attributeCheckVolumeLabel(currentEntry.fileAttributes) && attributeCheckSystem(currentEntry.fileAttributes) && attributeCheckHidden(currentEntry.fileAttributes) && attributeCheckReadOnly(currentEntry.fileAttributes)){
                /* 10 bytes from 0x0B */
                /* 12 bytes from 0x0E */
                /* 4 bytes from 0x1C */
            }
            else{
            #endif
                printf(".");
                for(int i = 0; i < 3; ++i){
                    printf("%c", currentEntry.shortExtension[i]);
                }
                printf("\n");
            //}

            /* File name */
            /* File Atributes */
            /* 0x0B 1 File Attributes, mask 0x01 read only, mask 0x02 hidden, mask 0x04 system, mask 0x08 volume label, mask 0x10 subdirectory, mask 0x20 archive, mask 0x40 device, mask 0x80 reserved */
            printf("File Attributes: ");
            if(currentEntry.fileAttributes){
                if(attributeCheckReadOnly(currentEntry.fileAttributes)){
                    printf("RO ");
                }
                if(attributeCheckHidden(currentEntry.fileAttributes)){
                    printf("Hidden ");
                }
                if(attributeCheckSystem(currentEntry.fileAttributes)){
                    printf("Sys ");
                }
                if(attributeCheckVolumeLabel(currentEntry.fileAttributes)){
                    printf("Vol. label ");
                }
                if(attributeCheckSubdirectory(currentEntry.fileAttributes)){
                    printf("subdir ");
                }
                if(attributeCheckArchive(currentEntry.fileAttributes)){
                    printf("archive ");
                }
                if(attributeCheckDevice(currentEntry.fileAttributes)){
                    printf("device ");
                }
            }
            printf("\n");

            printf("Create time: %02u/%02u/%02u ", translateYear(currentEntry.createDate), translateMonth(currentEntry.createDate), translateDay(currentEntry.createDate));
            printf("%02u:%02u:%02u.%03u\n", translateHour(currentEntry.createTime), translateMinutes(currentEntry.createTime), translateSeconds(currentEntry.createTime, currentEntry.createMillis), translateMillis(currentEntry.createMillis));

            printf("Access date: %02u/%02u/%02u\n", translateYear(currentEntry.lastAccessDate), translateMonth(currentEntry.lastAccessDate), translateDay(currentEntry.lastAccessDate));

            printf("Extended attributes: %u\n", currentEntry.extendedArtibutes);

            printf("Modify time: %02u/%02u/%02u ", translateYear(currentEntry.lastModifiedDate), translateMonth(currentEntry.lastModifiedDate), translateDay(currentEntry.lastModifiedDate));
            printf("%02u:%02u:%02u.%03u\n", translateHour(currentEntry.lastModifiedTime), translateMinutes(currentEntry.lastModifiedTime), translateSeconds(currentEntry.lastModifiedTime, 0), 0);

            printf("Start cluster: %u\n", currentEntry.firstLocation);

            printf("Bytes: %u\n", currentEntry.sizeOfFile);
        }
    }
    
    if(printFileContents){
        int currentClusterEntry = currentEntry.firstLocation;
        int sizeOfFile = currentEntry.sizeOfFile;
        uint8_t keepGoing = 1;
        int size = 0;
        while(keepGoing){
            int currentByte = 0;
            while( size < sizeOfFile && currentByte < sectorsPerCluster*bytesPerSector){
                char charByte = *(char *)(DataRegionOffset + (currentClusterEntry-2)*sectorsPerCluster*bytesPerSector+currentByte);
                printf("%c", charByte);
                ++size;
                ++currentByte;
            }
            currentClusterEntry = *(uint16_t *)(FATregionOffset + currentClusterEntry*2);
            if(currentClusterEntry < EOC &&(currentClusterEntry > 1)){
                //printf("\nNew Cluster: %d\n", currentClusterEntry);
            }
            else{
                keepGoing = 0;
            }
        }
    }

    int numRootFiles = 0;
    if(testNumEntries | testSpaceUsage | testLargestFile | testCookie | testNumDirLevels | testOldestFile){
        int currentLocation = 0;
        uint8_t stayInLoop = 1;
        struct DirectoryEntry currentEntry;
        while(stayInLoop){
            if(currentLocation < maxRootDirectoryEntries){
                populateDirectoryStruct(&currentEntry, rootDirectoryOffset+currentLocation*32);
                if (((unsigned char)currentEntry.shortFileName[0]!=0xE5)&&((unsigned char)currentEntry.shortFileName[0]!=0x2E)){
                    sizeOfFiles += currentEntry.sizeOfFile;
                }
                ++currentLocation;
                char * currentCompareString = (char * )malloc(sizeof(char)*(14));
                strcpy(currentCompareString, "/");
                strcat(currentCompareString, currentEntry.shortFileName);
                trimwhitespace(currentCompareString);
                if (currentEntry.shortExtension != NULL){
                    
                }
                if(currentCompareString[0] == 0x05){
                    currentCompareString[0] = 0xE5;
                }
                if(attributeCheckArchive(currentEntry.fileAttributes) && ((unsigned char)currentEntry.shortFileName[0]!=0xE5)){
                    ++numFiles;
                    ++numRootFiles;
                    if(currentEntry.shortExtension[0]!=' '){
                        strcat(currentCompareString, ".");
                        strcat(currentCompareString, currentEntry.shortExtension);
                    }
                    trimwhitespace(currentCompareString);

                
                    if(currentEntry.sizeOfFile > largestFileSize&&((unsigned char)currentEntry.shortFileName[0]!=0xE5)&&((unsigned char)currentEntry.shortFileName[0]!=0x2E)){
                        largestFileSize = currentEntry.sizeOfFile;
                        largestFileString = (char * )malloc(sizeof(char)*(14));
                        strcpy(largestFileString, currentCompareString);
                    }
                    uint64_t pseudoTimestamp = (currentEntry.createDate << 24) + (currentEntry.createTime << 8) + currentEntry.createMillis;
                    if( pseudoTimestamp < oldestFileTime&&((unsigned char)currentEntry.shortFileName[0]!=0xE5)&&((unsigned char)currentEntry.shortFileName[0]!=0x2E)){
                        oldestFileTime = pseudoTimestamp;
                        oldestFileString = (char * )malloc(sizeof(char)*(14));
                        strcpy(oldestFileString, currentCompareString);
                    }
                    int currentClusterEntry = currentEntry.firstLocation;
                    int sizeOfFile = currentEntry.sizeOfFile;
                    uint8_t keepGoing = 1;
                    uint8_t correctChars = 0;
                    int size = 0;
                    char cookie[15];
                    strcpy(cookie, "COS 421 cookie");
                    //printf("%s\n", currentCompareString);
                    if(currentClusterEntry < EOC && (currentClusterEntry > 1)){
                        //printf("New Cluster: %d\n", currentClusterEntry);
                    }
                    while(keepGoing){
                        int currentByte = 0;
                        while( size < sizeOfFile && currentByte < sectorsPerCluster*bytesPerSector){
                            char charByte = *(char *)(DataRegionOffset + (currentClusterEntry-2)*sectorsPerCluster*bytesPerSector+currentByte);
                            if(charByte == cookie[correctChars]){ /* not triggering */
                                ++correctChars;
                            }
                            else{
                                correctChars = 0;
                            }
                            if (correctChars == 14){
                                cookieCluster = currentEntry.firstLocation;
                                cookieFile = (char * )malloc(sizeof(char)*(14));
                                strcpy(cookieFile, currentCompareString);
                                keepGoing = 0;
                                size = sizeOfFile;
                            }
                            ++size;
                            ++currentByte;
                        }
                        
                        currentClusterEntry = *(uint16_t *)(FATregionOffset + currentClusterEntry*2);
                        if(currentClusterEntry < EOC &&(currentClusterEntry > 1)){
                            //printf("New Cluster: %d\n", currentClusterEntry);
                        }
                        else{
                            keepGoing = 0;
                        }
                    }
                    

                }
                if(attributeCheckSubdirectory(currentEntry.fileAttributes)&&(currentEntry.sizeOfFile==0)){
                    if((strncmp(currentEntry.shortFileName, ".",1)!=0)&&((unsigned char)currentEntry.shortFileName[0]!=0xE5)){
                        //printf("New Cluster: %d\n", currentEntry.firstLocation);
                        ++numDir;
                        traverseDirectory(&currentEntry, DataRegionOffset, FATregionOffset, bytesPerSector*sectorsPerCluster, currentCompareString, 2);
                    }
                }
                free(currentCompareString);
            }
            else{ /* not in this cluster, look in next */
                stayInLoop = 0;
                isValidDirectory = 0;

            }
        }
    }

    numberUsedClusters = 0;
    for (int i = 4; i <  sectorsPerFAT*bytesPerSector; i+=2){
        uint16_t currentCluster =*(uint16_t *)(FATregionOffset + i);
        if(currentCluster > 1){
            ++numberUsedClusters;
        }
    }

    if(testNumEntries){
        printf("Number of files in root directory: %d\n", numRootFiles);
        printf("Number of files in the file system: %d\n", numFiles);
        printf("Number of directories in the file system: %d\n", numDir);
    }
    if (testSpaceUsage){
        uint32_t totalCapacity = bytesPerSector*totalSectors;
        uint32_t spaceAllocated = numberUsedClusters*sectorsPerCluster*bytesPerSector;
        printf("Total capacity of the file system: %d\n", totalCapacity);
        printf("Total allocated space: %d\n", spaceAllocated); //+ reservedSectors*bytesPerSector+numFATs*sectorsPerFAT*bytesPerSector);
        printf("Total size of files: %d\n", sizeOfFiles);
        printf("Unused, but allocated, space (for files): %d\n", spaceAllocated-sizeOfFiles);
        printf("Unallocated space: %d\n", totalCapacity-spaceAllocated);
    }
    if(testLargestFile){
        printf("Largest file (%d bytes): %s\n", largestFileSize, largestFileString);
    }
    if(testCookie){
        printf("Path to file with cookie: %s\n", cookieFile);
        printf("Starting cluster for file with cookie: %d\n", cookieCluster);
    }
    if(testNumDirLevels){
        printf("Directory hierarchy levels: %d\n", maxDirDepth);
    }
    if(testOldestFile){
        printf("Oldest file: %s\n", oldestFileString);
    }
    int unmap_result = munmap(region, pageSize);
    close(fileDescriptor);
    return 0;
}

