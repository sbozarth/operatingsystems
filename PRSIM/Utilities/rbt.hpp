/************************************/
/* Authored by Daniel Scott Bozarth */
/************************************/
#ifndef RBT_HEADER_FILE
#define RBT_HEADER_FILE
#define VERSION 0.3

#include <iostream> 
#include <queue> 

enum COLOR { RED, BLACK }; 
  
class Node { 
public: 
    int val; 
    COLOR color; 
    Node *left, *right, *parent; 
    
    Node(unsigned int val) : val(val) { 
        parent = left = right = NULL; 
        color = RED; 
    } 

    Node *uncle();
    bool isOnLeft(); 
    Node *sibling();
    void moveDown(Node *nParent);
    bool hasRedChild();
};
  
class RBTree {
    int size = 0;
    Node *root; 

    void leftRotate(Node *x);
    void rightRotate(Node *x);
    void swapColors(Node *x1, Node *x2);
    void swapValues(Node *u, Node *v);
    void fixRedRed(Node *x);
    Node *successor(Node *x);
    Node *BSTreplace(Node *x);
    void removeNode(Node *v);
    void fixDoubleBlack(Node *x);
    void levelOrder(Node *x);
    void inorder(Node *x);
  
public: 
    RBTree();
  
    Node *getRoot();
    int getSize();
    Node *search(unsigned int n);
    void removeRandom();
    void insertVal(unsigned int n); 
    void deleteVal(unsigned int n);
    void deleteNode(Node *v);
    void printInOrder();
    void printLevelOrder();
}; 

#endif