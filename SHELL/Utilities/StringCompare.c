
int strcmpnn (const char * s1, const char * s2)
{
  char compare;
  do
    {
      compare = *s1;
      if (compare == '\n')
          compare = 0;
      if (compare != *s2)
          return 1;
      s1++;
      s2++;
    } while (compare); /* already checked *s2 is equal */
  return 0;
}