/************************************/
/* Authored by Daniel Scott Bozarth */
/************************************/
#define VERSION_NUM 0.8
#include "shell.h"
#include "Utilities/StringCompare.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <signal.h>
#include <errno.h>

#define BUFFER_SIZE 128
char delimiters[] = " \n";
char block[] = "'\"";

void handle_sigchld(int sig) {
    int olderrno = errno;

    while(waitpid(-1,NULL,0)>0){

    }
    if (errno != ECHILD){
        perror("SIG HANDLE ERROR");
    }
    errno = olderrno;
}


char *strmbtok ( char *input, char *delimit, char *openblock, char *closeblock) {
    static char *token = NULL;
    char *head = NULL;
    char *block = NULL;
    int haveBlock = 0;
    int inBlock = 0;
    int inBlockIndex = 0;

    if ( input != NULL) {
        token = input;
        head = input;
    }
    else {
        head = token;
        if ( *token == '\0') {
            head = NULL;
        }
    }

    while ( *token != '\0') {
        if ( inBlock) {
            if ( closeblock[inBlockIndex] == *token) {
                inBlock = 0;
                *token = '\0';
            }
            token++;
            continue;
        }
        if ( ( block = strchr ( openblock, *token)) != NULL) {
            inBlock = 1;
            haveBlock = 1;
            inBlockIndex = block - openblock;
            *token = '\0';
            token++;
            continue;
        }
        if ( strchr ( delimit, *token) != NULL) {
            *token = '\0';
            token++;
            break;
        }
        token++;
    }
    return haveBlock ? head+1 : head;
}


int shellLogic(int myargc, char * myargv[]){
    #ifdef DEBUG_PRINT
    printf("Call Argc %d\n", myargc);
    for(int i = 0; i < myargc; ++i){
        printf("Call Arg %d: %s\n", i, myargv[i]);
    }
    #endif
    int32_t rc = -2;
    int8_t doFork = 0;
    int8_t isPipe = 0;
    int fd[2];
    int childargc;
    char ** childargv;
    int newFDinput = -1;
    int newFDoutput = -1;
    for (int l = 0; l < myargc; ++l){
        if(strcmp(myargv[l],"&")==0){
            childargc = myargc-l-1;
            childargv = malloc((childargc+1) * sizeof(char*));
            childargv = &myargv[l+1];
            myargc = l;
            myargv[l] = NULL;
            doFork = 1;
        }

        else if(strcmp(myargv[l],"|")==0){
            childargc = myargc-l-1;
            childargv = malloc((childargc+1) * sizeof(char*));
            childargv = &myargv[l+1];
            myargc = l;
            myargv[l] = NULL;
            doFork = 1;
            isPipe = 1;
            pipe(fd);
        }

    }
    for (int m = 0; m < myargc; ++m){
        if(strcmp(myargv[m], "<")==0){
            myargv[m] = NULL;
            ++m;
            newFDinput = open(myargv[m], O_RDONLY);
            //dup2(newFDinput, 0);
            myargv[m] = NULL;
            myargc -= 2;
        }
        else if(strcmp(myargv[m], ">")==0){
            myargv[m] = NULL;
            ++m;
            newFDoutput = open(myargv[m], O_CREAT|O_RDWR);
            //dup2(newFDoutput, 1);
            myargv[m] = NULL;
            myargc -= 2;
        }
    }
    if (doFork){
        rc = fork();
    }
    if (rc == -1){ /* error */;
        perror("FORK ERROR");
        exit(1);
    }
    else if ((rc == 0) || (rc == -2)){ /* child */
        #ifdef DEBUG_PRINT
        printf("Execute argc %d\n", myargc);
        for (int i = 0; i < myargc; ++i){
            printf("Execute arg %d: %s\n", i, myargv[i]);
        }
        #endif
        if (isPipe){
            close(fd[0]);
            dup2(fd[1], 1);
        }
        if (newFDinput != -1){
            dup2(newFDinput, 0);
        }
        else if (newFDoutput != -1){
            dup2(newFDoutput, 1);
        }
        if((strncmp(myargv[0],"/",1)==0)|(strncmp(myargv[0],"./",2)==0)|(strncmp(myargv[0],"../",3)==0)){
            execve(myargv[0], myargv, __environ);
        }
        else if(strchr(myargv[0],'/') != NULL){
            execve(myargv[0], myargv, __environ);
        }
        else{
            char * tempEnvArgs = malloc(strlen(getenv("PATH"))+1);
            strcpy(tempEnvArgs, getenv("PATH"));
            char * pathToken;
            pathToken = strtok(tempEnvArgs, ":");
            while (pathToken != NULL){/* get ammount of arguments */
                char * tempPath;
                tempPath = malloc(strlen(pathToken)+1);
                strcpy(tempPath, pathToken);
                strcat(tempPath,"/");
                strcat(tempPath, myargv[0]);
                execve(tempPath, myargv, __environ);
                if(errno != ENOENT){
                    break;
                }
                pathToken = strtok(NULL, ":");
            }
        }
        perror("PARENT ERROR");
        return 0;   
    }
    else{ /* parent */
        if (isPipe){
            close(fd[1]);
            dup2(fd[0], 0);
        }
        if((childargc > 0) && strcmp(childargv[0],"\0")){
            shellLogic(childargc, childargv);
        }
        return 1;
    }
}

int main(int argc, char * argv[]){
    int8_t runShell = 1;
    int8_t waitForChild = 1;
    struct sigaction sa;
    sa.sa_handler = &handle_sigchld;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART | SA_NOCLDSTOP;
    if (sigaction(SIGCHLD, &sa, 0) == -1) {
        perror(0);
        exit(1);
    }
    while(runShell){
        char buffer[BUFFER_SIZE];
        printf("$ ");
        fgets(buffer, BUFFER_SIZE, stdin);
        char * token;
        char passoffBuffer[BUFFER_SIZE];
        passoffBuffer[0] = '\0';
        /* count words */
        char * tempBuffer;
        tempBuffer = malloc(strlen(buffer) * sizeof(char));
        strcpy(tempBuffer, buffer);
        int8_t childargc = 0;
        token = strmbtok (tempBuffer, delimiters, block, block);
        while (token != NULL){/* get ammount of arguments */
            ++childargc;
            token = strmbtok (NULL, delimiters, block, block);
        }
        /* count words */
        char ** childargv;
        childargv = malloc((childargc+1) * sizeof(char*));
        token = strmbtok (buffer, delimiters, block, block);
        int8_t i = 0;
        int8_t noMatch = 0;
        int8_t afterAnd = 0;
        int pfd[2];
        while (token != NULL){
            if(!afterAnd){
                childargv[i] = token;
            }
            else{
                strcat(passoffBuffer, token);
                strcat(passoffBuffer," ");
            }
            ++i;
            token = strmbtok (NULL, delimiters, block, block);
        }
        childargv[childargc] = 0; /* need a null command at the end */
        #ifdef DEBUG_PRINT
        printf("Shell Argc %d\n", childargc);
        for(int i = 0; i < childargc; ++i){
            printf("Shell Arg %d: %s\n", i, childargv[i]);
        }
        #endif
        if (strcmp(childargv[0],"\0")!=0){ /* if no command */
            if(strcmp(childargv[0], "exit")==0){
                runShell = 0;
                exit(0);
            }
            else if(strcmp(childargv[0], "cd")==0){
            if(chdir(childargv[1]) != 0){
                    perror("CD ERROR");
                }
            }
            else if(strcmp(childargv[0], "pwd")==0){
                char currentDirectory[PATH_MAX];
                if(getcwd(currentDirectory, sizeof(currentDirectory))==NULL){
                    perror("PWD ERROR");
                }
                else{
                    printf("%s\n", currentDirectory);
                }
            }
            else if (strcmp(childargv[0], "version")==0){
                printf("Made by Daniel Scott Bozarth, version %2f\n", VERSION_NUM);
            }
            else{
                noMatch = 1;
            }
        }
        waitForChild = 1;
        for(int i = 0; i < childargc; ++i){
            if(strcmp(childargv[i],"&")==0){
                waitForChild = 0;
                break;
            }
        }
        if (noMatch){
            int rc = fork();
            if (rc < 0){ /* error */;
                perror("FORK ERROR");
                runShell = 0;
                exit(1);
            }
            else if (rc == 0){ /* child */
                if((childargc > 0) && strcmp(childargv[0],"\0")){
                    shellLogic(childargc, childargv);
                }
                exit(0);
            }
            else{ /* parent */
                if (waitForChild){
                    int32_t rc_wait;
                    int32_t rc_status;
                    while (wait(&rc_status)!=-1); /* Wait for child to finish execution */
                }
            }
        }
    }
    return 0;
}
