# Shell

Simple shell program made by Daniel Scott Bozarth for cos421

# Contents
Shell.c - The framework code fore the shell

# Compile

Make shell:

`make tush`

or

`make`

Clean Directory:

`make clean`

On linux:

`gcc -c -o shell.o shell.c`

`gcc - Wall -o tush shell.o`

# Run

`./tush <arguments>`

# Notes

