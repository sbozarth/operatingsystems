# FS

FAT16 reader

# Contents

FIXME

# Known Issues

Does not support long filenames

Writing to file being implemented

CSE computers give permission denied errors for .exp files on runtest unless i run them manually a single time. 

# MS1

Completed on time

# MS2

Completed on time

# MS3

Completed on time

# MS4

All but check cookie and check space usage completed before class.

Check cookie completed soon after class. Majority of space usage done in a form after class. Changed implementation with Dr. Geisler on May 9 which solved two edge cases.

# MS5

Discovered missing support for long filenames

In Progress: 10 fails, 1042 passes.

# Compile

Make Page Replacement Simulator:

`make fs`

or

`make`

Clean Directory:

`make clean`


# Run

FIXME

test mmap

`./fs --test-mmap --image <image>`

test boot sector

`./fs --test-boot-sector --image <image>`

test root directory

`./fs --test-directory-entry <root directory #> --image <image>`

# Notes

