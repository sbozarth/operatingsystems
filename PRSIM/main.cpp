/************************************/
/* Authored by Daniel Scott Bozarth */
/************************************/

/* Instruction Format */
/* <instruction> <address> <size>*/

/* pages start at 0x0 and next page starts at 0x0 + 1<<PAGE_SIZE_BITS */
/* If a memory value goes between two pages we need them both */
/* When we get two, need to make sure getting one doest evict the other */
/* I know some of the code looks like garbage, but it is for optimization */

#define PAGE_SIZE_BITS 12
#define VERSION_NUM 0.1

#include "prsim.h"
#include "utilities/rbt.hpp"
//#include "utilities/queue.h"
#include <getopt.h>
#include <string.h>
#include <time.h>
#include <queue>

#define BUFFER_SIZE 32
char delimiters[] = " ,\n";
int isDebug = 0;

int randomInsert(unsigned int requestedPage, int numPages, RBTree& sortedTree){
    int fault = 0;
    int insertIsTrue = 0;
    Node * tempNode = sortedTree.search(requestedPage);
    if (tempNode == NULL){ /* empty tree */
        insertIsTrue = 1;
    }
    else if (tempNode->val != requestedPage){/* not in tree */
        insertIsTrue = 1;
    }
    if (insertIsTrue){
        fault = 1;
        if(numPages > sortedTree.getSize()){ /* space in tree */
            sortedTree.insertVal(requestedPage);
        }
        else{ /* tree is full */
            sortedTree.removeRandom();
            sortedTree.insertVal(requestedPage);
        }
    }
    return fault;
}

unsigned long int randomReplacement(int isSweep, int numPages){
    srand (time(0));
    unsigned long int numFaults = 0;
    char buffer[BUFFER_SIZE];
    char* instruction;
    char* address;
    char* size;
    if (isSweep){
        RBTree tree1, tree2, tree5, tree10, tree20, tree50, tree100, tree200, tree500, tree1000, tree2000, tree5000, tree10000, tree20000;
        unsigned int numFaults1, numFaults2, numFaults5, numFaults10, numFaults20, numFaults50, numFaults100, numFaults200, numFaults500, numFaults1000, numFaults2000, numFaults5000, numFaults10000, numFaults20000;
        numFaults1 = numFaults2 = numFaults5 = numFaults10 = numFaults20 = numFaults50 = numFaults100 = numFaults200 = numFaults500 = numFaults1000 = numFaults2000 = numFaults5000 = numFaults10000 = numFaults20000 = 0;
        while (fgets(buffer, BUFFER_SIZE, stdin)!=NULL){
            instruction = strtok(buffer,delimiters);
            if ((instruction != NULL)&&!(strcmp(instruction, "I")&&strcmp(instruction, "S")&&strcmp(instruction, "L")&&strcmp(instruction, "M"))){ /* Not NULL, and valid instruction */
                address = strtok(NULL, delimiters);
                if (address != NULL){ /* Not NULL */
                    size = strtok(NULL, delimiters);
                    if(size != NULL){ /* Not NULL */
                        unsigned long int addressNum = strtol(address, NULL, 16);
                        unsigned int pageFirst = addressNum>>PAGE_SIZE_BITS;
                        unsigned int pageSecond = (addressNum+strtol(size, NULL, 10)-1)>>PAGE_SIZE_BITS;
                        numFaults1 += randomInsert(pageFirst, 1, tree1); /* Check first page */
                        numFaults2 += randomInsert(pageFirst, 2, tree2); /* Check first page */
                        numFaults5 += randomInsert(pageFirst, 5, tree5); /* Check first page */
                        numFaults10 += randomInsert(pageFirst, 10, tree10); /* Check first page */
                        numFaults20 += randomInsert(pageFirst, 20, tree20); /* Check first page */
                        numFaults50 += randomInsert(pageFirst, 50, tree50); /* Check first page */
                        numFaults100 += randomInsert(pageFirst, 100, tree100); /* Check first page */
                        numFaults200 += randomInsert(pageFirst, 200, tree200); /* Check first page */
                        numFaults500 += randomInsert(pageFirst, 500, tree500); /* Check first page */
                        numFaults1000 += randomInsert(pageFirst, 1000, tree1000); /* Check first page */
                        numFaults2000 += randomInsert(pageFirst, 2000, tree2000); /* Check first page */
                        numFaults5000 += randomInsert(pageFirst, 5000, tree5000); /* Check first page */
                        numFaults10000 += randomInsert(pageFirst, 10000, tree10000); /* Check first page */
                        numFaults20000 += randomInsert(pageFirst, 20000, tree20000); /* Check first page */
                        if (pageSecond!=pageFirst){ /* if we need a second page */
                            numFaults1 += randomInsert(pageSecond, 1, tree1);
                            numFaults2 += randomInsert(pageSecond, 2, tree2);
                            numFaults5 += randomInsert(pageSecond, 5, tree5);
                            numFaults10 += randomInsert(pageSecond, 10, tree10);
                            numFaults20 += randomInsert(pageSecond, 20, tree20);
                            numFaults50 += randomInsert(pageSecond, 50, tree50);
                            numFaults100 += randomInsert(pageSecond, 100, tree100);
                            numFaults200 += randomInsert(pageSecond, 200, tree200);
                            numFaults500 += randomInsert(pageSecond, 500, tree500);
                            numFaults1000 += randomInsert(pageSecond, 1000, tree1000);
                            numFaults2000 += randomInsert(pageSecond, 2000, tree2000);
                            numFaults5000 += randomInsert(pageSecond, 5000, tree5000);
                            numFaults10000 += randomInsert(pageSecond, 10000, tree10000);
                            numFaults20000 += randomInsert(pageSecond, 20000, tree20000);
                        }
                    }
                }
            }
            memset(buffer,0,BUFFER_SIZE);
        }
        std::cout << "random" << " 1" << ": " << numFaults1 << std::endl;
        std::cout << "random" << " 2" << ": " << numFaults2 << std::endl;
        std::cout << "random" << " 5" << ": " << numFaults5 << std::endl;
        std::cout << "random" << " 10" << ": " << numFaults10 << std::endl;
        std::cout << "random" << " 20" << ": " << numFaults20 << std::endl;
        std::cout << "random" << " 50" << ": " << numFaults50 << std::endl;
        std::cout << "random" << " 100" << ": " << numFaults100 << std::endl;
        std::cout << "random" << " 200" << ": " << numFaults200 << std::endl;
        std::cout << "random" << " 500" << ": " << numFaults500 << std::endl;
        std::cout << "random" << " 1000" << ": " << numFaults1000 << std::endl;
        std::cout << "random" << " 2000" << ": " << numFaults2000 << std::endl;
        std::cout << "random" << " 5000" << ": " << numFaults5000 << std::endl;
        std::cout << "random" << " 10000" << ": " << numFaults10000 << std::endl;
        std::cout << "random" << " 20000" << ": " << numFaults20000 << std::endl;
        numFaults = numFaults1 + numFaults2 + numFaults5 + numFaults10 + numFaults20 + numFaults50 + numFaults100 + numFaults200 + numFaults500 + numFaults1000 + numFaults2000 + numFaults5000 + numFaults10000 + numFaults20000;
    }
    else{
        RBTree sortedTree;
        while (fgets(buffer, BUFFER_SIZE, stdin)!=NULL){
            instruction = strtok(buffer,delimiters);
            if ((instruction != NULL)&&!(strcmp(instruction, "I")&&strcmp(instruction, "S")&&strcmp(instruction, "L")&&strcmp(instruction, "M"))){ /* Not NULL, and valid instruction */
                address = strtok(NULL, delimiters);
                if (address != NULL){ /* Not NULL */
                    size = strtok(NULL, delimiters);
                    if(size != NULL){ /* Not NULL */
                        unsigned long int addressNum = strtol(address, NULL, 16);
                        unsigned int pageFirst = addressNum>>PAGE_SIZE_BITS;
                        unsigned int pageSecond = (addressNum+strtol(size, NULL, 10)-1)>>PAGE_SIZE_BITS;
                        numFaults += randomInsert(pageFirst, numPages, sortedTree); /* Check first page */
                        if (pageSecond!=pageFirst){/* if we need a second page */
                            numFaults += randomInsert(pageSecond, numPages, sortedTree);
                        }
                    }
                }
            }
            memset(buffer,0,BUFFER_SIZE);
        }
        std::cout << "Random " << numPages << ": " << numFaults << std::endl;
    }
    return numFaults;
}

int FIFOinsert(unsigned int requestedPage, int numPages, RBTree& sortedTree, std::queue<unsigned int>& replacemetQueue){
    int fault = 0;
    int insertIsTrue = 0;
    Node * tempNode = sortedTree.search(requestedPage);
    if (isDebug){
        std::cout<<requestedPage;
    }
    if (tempNode == NULL){ /* empty tree */
        insertIsTrue = 1;
        if (isDebug){
            std::cout<<" miss -> ? ";
        }
    }
    else if (tempNode->val != requestedPage){/* not in tree */
        insertIsTrue = 1;
        if (isDebug){
            std::cout<<" miss -> ? ";
        }
    }
    else{
        if (isDebug){
            std::cout<<" hit"<<std::endl;
        }
    }
    if (insertIsTrue){
        fault = 1;
        if(numPages > sortedTree.getSize()){ /* space in tree */
            sortedTree.insertVal(requestedPage);
            replacemetQueue.push(requestedPage);
            if(isDebug){
                unsigned int valueToBeDeleted = replacemetQueue.front();
                std::cout<<"("<<valueToBeDeleted<<")"<<std::endl;
            }
        }
        else{ /* tree is full */
            unsigned int valueToBeDeleted = replacemetQueue.front();
            if(isDebug){
                std::cout<<"("<<valueToBeDeleted<<")"<<std::endl;
            }
            sortedTree.deleteVal(valueToBeDeleted);
            replacemetQueue.pop();
            sortedTree.insertVal(requestedPage);
            replacemetQueue.push(requestedPage);
        }
    }
    return fault;
}

unsigned long int FIFOreplacement(int isSweep, int numPages){
    unsigned long int numFaults = 0;
    char buffer[BUFFER_SIZE];
    char * instruction;
    char * address;
    char * size;
    if (isSweep){
        RBTree tree1, tree2, tree5, tree10, tree20, tree50, tree100, tree200, tree500, tree1000, tree2000, tree5000, tree10000, tree20000;
        unsigned int numFaults1, numFaults2, numFaults5, numFaults10, numFaults20, numFaults50, numFaults100, numFaults200, numFaults500, numFaults1000, numFaults2000, numFaults5000, numFaults10000, numFaults20000;
        numFaults1 = numFaults2 = numFaults5 = numFaults10 = numFaults20 = numFaults50 = numFaults100 = numFaults200 = numFaults500 = numFaults1000 = numFaults2000 = numFaults5000 = numFaults10000 = numFaults20000 = 0;
        std::queue<unsigned int> replacemetQueue1, replacemetQueue2, replacemetQueue5, replacemetQueue10, replacemetQueue20, replacemetQueue50, replacemetQueue100, replacemetQueue200, replacemetQueue500, replacemetQueue1000, replacemetQueue2000, replacemetQueue5000, replacemetQueue10000, replacemetQueue20000;
        while (fgets(buffer, BUFFER_SIZE, stdin)!=NULL){
            instruction = strtok(buffer,delimiters);
            if ((instruction != NULL)&&!(strcmp(instruction, "I")&&strcmp(instruction, "S")&&strcmp(instruction, "L")&&strcmp(instruction, "M"))){ /* Not NULL, and valid instruction */
                address = strtok(NULL, delimiters);
                if (address != NULL){ /* Not NULL */
                    size = strtok(NULL, delimiters);
                    if(size != NULL){ /* Not NULL */
                        unsigned long int addressNum = strtol(address, NULL, 16);
                        unsigned int pageFirst = addressNum>>PAGE_SIZE_BITS;
                        unsigned int pageSecond = (addressNum+strtol(size, NULL, 10)-1)>>PAGE_SIZE_BITS;
                        numFaults1 += FIFOinsert(pageFirst, 1, tree1, replacemetQueue1); /* Check first page */
                        numFaults2 += FIFOinsert(pageFirst, 2, tree2, replacemetQueue2); /* Check first page */
                        numFaults5 += FIFOinsert(pageFirst, 5, tree5, replacemetQueue5); /* Check first page */
                        numFaults10 += FIFOinsert(pageFirst, 10, tree10, replacemetQueue10); /* Check first page */
                        numFaults20 += FIFOinsert(pageFirst, 20, tree20, replacemetQueue20); /* Check first page */
                        numFaults50 += FIFOinsert(pageFirst, 50, tree50, replacemetQueue50); /* Check first page */
                        numFaults100 += FIFOinsert(pageFirst, 100, tree100, replacemetQueue100); /* Check first page */
                        numFaults200 += FIFOinsert(pageFirst, 200, tree200, replacemetQueue200); /* Check first page */
                        numFaults500 += FIFOinsert(pageFirst, 500, tree500, replacemetQueue500); /* Check first page */
                        numFaults1000 += FIFOinsert(pageFirst, 1000, tree1000, replacemetQueue1000); /* Check first page */
                        numFaults2000 += FIFOinsert(pageFirst, 2000, tree2000, replacemetQueue2000); /* Check first page */
                        numFaults5000 += FIFOinsert(pageFirst, 5000, tree5000, replacemetQueue5000); /* Check first page */
                        numFaults10000 += FIFOinsert(pageFirst, 10000, tree10000, replacemetQueue10000); /* Check first page */
                        numFaults20000 += FIFOinsert(pageFirst, 20000, tree20000, replacemetQueue20000); /* Check first page */
                        if (pageSecond!=pageFirst){ /* if we need a second page */
                            numFaults1 += FIFOinsert(pageSecond, 1, tree1, replacemetQueue1);
                            numFaults2 += FIFOinsert(pageSecond, 2, tree2, replacemetQueue2);
                            numFaults5 += FIFOinsert(pageSecond, 5, tree5, replacemetQueue5);
                            numFaults10 += FIFOinsert(pageSecond, 10, tree10, replacemetQueue10);
                            numFaults20 += FIFOinsert(pageSecond, 20, tree20, replacemetQueue20);
                            numFaults50 += FIFOinsert(pageSecond, 50, tree50, replacemetQueue50);
                            numFaults100 += FIFOinsert(pageSecond, 100, tree100, replacemetQueue100);
                            numFaults200 += FIFOinsert(pageSecond, 200, tree200, replacemetQueue200);
                            numFaults500 += FIFOinsert(pageSecond, 500, tree500, replacemetQueue500);
                            numFaults1000 += FIFOinsert(pageSecond, 1000, tree1000, replacemetQueue1000);
                            numFaults2000 += FIFOinsert(pageSecond, 2000, tree2000, replacemetQueue2000);
                            numFaults5000 += FIFOinsert(pageSecond, 5000, tree5000, replacemetQueue5000);
                            numFaults10000 += FIFOinsert(pageSecond, 10000, tree10000, replacemetQueue10000);
                            numFaults20000 += FIFOinsert(pageSecond, 20000, tree20000, replacemetQueue20000);
                        }
                    }
                }
            }
            memset(buffer,0,BUFFER_SIZE);
        }
        std::cout << "FIFO" << " 1" << ": " << numFaults1 << std::endl;
        std::cout << "FIFO" << " 2" << ": " << numFaults2 << std::endl;
        std::cout << "FIFO" << " 5" << ": " << numFaults5 << std::endl;
        std::cout << "FIFO" << " 10" << ": " << numFaults10 << std::endl;
        std::cout << "FIFO" << " 20" << ": " << numFaults20 << std::endl;
        std::cout << "FIFO" << " 50" << ": " << numFaults50 << std::endl;
        std::cout << "FIFO" << " 100" << ": " << numFaults100 << std::endl;
        std::cout << "FIFO" << " 200" << ": " << numFaults200 << std::endl;
        std::cout << "FIFO" << " 500" << ": " << numFaults500 << std::endl;
        std::cout << "FIFO" << " 1000" << ": " << numFaults1000 << std::endl;
        std::cout << "FIFO" << " 2000" << ": " << numFaults2000 << std::endl;
        std::cout << "FIFO" << " 5000" << ": " << numFaults5000 << std::endl;
        std::cout << "FIFO" << " 10000" << ": " << numFaults10000 << std::endl;
        std::cout << "FIFO" << " 20000" << ": " << numFaults20000 << std::endl;
        numFaults = numFaults1 + numFaults2 + numFaults5 + numFaults10 + numFaults20 + numFaults50 + numFaults100 + numFaults200 + numFaults500 + numFaults1000 + numFaults2000 + numFaults5000 + numFaults10000 + numFaults20000;
    }
    else{
        RBTree sortedTree;
        std::queue<unsigned int> replacemetQueue;
        while (fgets(buffer, BUFFER_SIZE, stdin)!=NULL){
            instruction = strtok(buffer,delimiters);
            if ((instruction != NULL)&&!(strcmp(instruction, "I")&&strcmp(instruction, "S")&&strcmp(instruction, "L")&&strcmp(instruction, "M"))){ /* Not NULL, and valid instruction */
                address = strtok(NULL, delimiters);
                if (address != NULL){ /* Not NULL */
                    size = strtok(NULL, delimiters);
                    if(size != NULL){ /* Not NULL */
                        unsigned long int addressNum = strtol(address, NULL, 16);
                        unsigned int pageFirst = addressNum>>PAGE_SIZE_BITS;
                        unsigned int pageSecond = (addressNum+strtol(size, NULL, 10)-1)>>PAGE_SIZE_BITS;
                        numFaults += FIFOinsert(pageFirst, numPages, sortedTree, replacemetQueue); /* Check first page */
                        if (pageSecond!=pageFirst){/* if we need a second page */
                            numFaults += FIFOinsert(pageSecond, numPages, sortedTree, replacemetQueue);
                        }
                    }
                }
            }
            memset(buffer,0,BUFFER_SIZE);
        }
        std::cout << "FIFO " << numPages << ": " << numFaults << std::endl;
    }
    return numFaults;
}

int MRUinsert(unsigned int requestedPage, int numPages, RBTree& sortedTree, unsigned int MRUval){
    int fault = 0;
    int insertIsTrue = 0;
    Node * tempNode = sortedTree.search(requestedPage);
    if (isDebug){
        printf("%d", requestedPage);
    }
    if (tempNode == NULL){ /* empty tree */
        insertIsTrue = 1;
        if (isDebug){
            std::cout<<" miss -> ? ";
        }
    }
    else if (tempNode->val != requestedPage){/* not in tree */
        insertIsTrue = 1;
        if (isDebug){
            std::cout<<" miss -> ? ";
        }
    }
    if (insertIsTrue){
        fault = 1;
        if(isDebug){
            std::cout<<"("<<MRUval<<")"<<std::endl;
        }
        if(numPages > sortedTree.getSize()){ /* space in tree */
            sortedTree.insertVal(requestedPage);
        }
        else{ /* tree is full */
            Node * remove = sortedTree.search(MRUval);
            sortedTree.deleteNode(remove);
            sortedTree.insertVal(requestedPage);
        }
    }
    return fault;
}

unsigned long int MRUreplacement(int isSweep, int numPages){
    unsigned long int numFaults = 0;
    char buffer[BUFFER_SIZE];
    char* instruction;
    char* address;
    char* size;
    unsigned int pageFirst = 0;
    unsigned int pageSecond = 0;
    if (isSweep){
        RBTree tree1, tree2, tree5, tree10, tree20, tree50, tree100, tree200, tree500, tree1000, tree2000, tree5000, tree10000, tree20000;
        unsigned int MRUval1, MRUval2, MRUval5, MRUval10, MRUval20, MRUval50, MRUval100, MRUval200, MRUval500, MRUval1000, MRUval2000, MRUval5000, MRUval10000, MRUval20000;
        unsigned int numFaults1, numFaults2, numFaults5, numFaults10, numFaults20, numFaults50, numFaults100, numFaults200, numFaults500, numFaults1000, numFaults2000, numFaults5000, numFaults10000, numFaults20000;
        numFaults1 = numFaults2 = numFaults5 = numFaults10 = numFaults20 = numFaults50 = numFaults100 = numFaults200 = numFaults500 = numFaults1000 = numFaults2000 = numFaults5000 = numFaults10000 = numFaults20000 = 0;
        while (fgets(buffer, BUFFER_SIZE, stdin)!=NULL){
            instruction = strtok(buffer,delimiters);
            if ((instruction != NULL)&&!(strcmp(instruction, "I")&&strcmp(instruction, "S")&&strcmp(instruction, "L")&&strcmp(instruction, "M"))){ /* Not NULL, and valid instruction */
                address = strtok(NULL, delimiters);
                if (address != NULL){ /* Not NULL */
                    size = strtok(NULL, delimiters);
                    if(size != NULL){ /* Not NULL */
                        unsigned long int addressNum = strtol(address, NULL, 16);
                        pageFirst = addressNum>>PAGE_SIZE_BITS;
                        pageSecond = (addressNum+strtol(size, NULL, 10)-1)>>PAGE_SIZE_BITS;
                        numFaults1 += MRUinsert(pageFirst, 1, tree1, MRUval1); 
                        numFaults2 += MRUinsert(pageFirst, 2, tree2, MRUval2); 
                        numFaults5 += MRUinsert(pageFirst, 5, tree5, MRUval5); 
                        numFaults10 += MRUinsert(pageFirst, 10, tree10, MRUval10); 
                        numFaults20 += MRUinsert(pageFirst, 20, tree20, MRUval20); 
                        numFaults50 += MRUinsert(pageFirst, 50, tree50, MRUval50); 
                        numFaults100 += MRUinsert(pageFirst, 100, tree100, MRUval100); 
                        numFaults200 += MRUinsert(pageFirst, 200, tree200, MRUval200); 
                        numFaults500 += MRUinsert(pageFirst, 500, tree500, MRUval500); 
                        numFaults1000 += MRUinsert(pageFirst, 1000, tree1000, MRUval1000); 
                        numFaults2000 += MRUinsert(pageFirst, 2000, tree2000, MRUval2000); 
                        numFaults5000 += MRUinsert(pageFirst, 5000, tree5000, MRUval5000); 
                        numFaults10000 += MRUinsert(pageFirst, 10000, tree10000, MRUval10000); 
                        numFaults20000 += MRUinsert(pageFirst, 20000, tree20000, MRUval20000); 
                        MRUval1 = MRUval2 = MRUval5 = MRUval10 = MRUval20 = MRUval50 = MRUval100 = MRUval200 = MRUval500 = MRUval1000 = MRUval2000 = MRUval5000 = MRUval10000 = MRUval20000 = pageFirst;
                        if (pageSecond!=pageFirst){ /* if we need a second page */
                            numFaults1 += MRUinsert( pageSecond, 1, tree1, MRUval1); 
                            numFaults2 += MRUinsert( pageSecond, 2, tree2, MRUval2); 
                            numFaults5 += MRUinsert( pageSecond, 5, tree5, MRUval5); 
                            numFaults10 += MRUinsert( pageSecond, 10, tree10, MRUval10); 
                            numFaults20 += MRUinsert( pageSecond, 20, tree20, MRUval20); 
                            numFaults50 += MRUinsert( pageSecond, 50, tree50, MRUval50); 
                            numFaults100 += MRUinsert( pageSecond, 100, tree100, MRUval100); 
                            numFaults200 += MRUinsert( pageSecond, 200, tree200, MRUval200); 
                            numFaults500 += MRUinsert( pageSecond, 500, tree500, MRUval500); 
                            numFaults1000 += MRUinsert( pageSecond, 1000, tree1000, MRUval1000); 
                            numFaults2000 += MRUinsert( pageSecond, 2000, tree2000, MRUval2000); 
                            numFaults5000 += MRUinsert( pageSecond, 5000, tree5000, MRUval5000); 
                            numFaults10000 += MRUinsert( pageSecond, 10000, tree10000, MRUval10000); 
                            numFaults20000 += MRUinsert( pageSecond, 20000, tree20000, MRUval20000); 
                            MRUval1 = MRUval2 = MRUval5 = MRUval10 = MRUval20 = MRUval50 = MRUval100 = MRUval200 = MRUval500 = MRUval1000 = MRUval2000 = MRUval5000 = MRUval10000 = MRUval20000 = pageSecond;
                        }
                    }
                }
            }
            memset(buffer,0,BUFFER_SIZE);
        }
        std::cout << "MRU" << " 1" << ": " << numFaults1 << std::endl;
        std::cout << "MRU" << " 2" << ": " << numFaults2 << std::endl;
        std::cout << "MRU" << " 5" << ": " << numFaults5 << std::endl;
        std::cout << "MRU" << " 10" << ": " << numFaults10 << std::endl;
        std::cout << "MRU" << " 20" << ": " << numFaults20 << std::endl;
        std::cout << "MRU" << " 50" << ": " << numFaults50 << std::endl;
        std::cout << "MRU" << " 100" << ": " << numFaults100 << std::endl;
        std::cout << "MRU" << " 200" << ": " << numFaults200 << std::endl;
        std::cout << "MRU" << " 500" << ": " << numFaults500 << std::endl;
        std::cout << "MRU" << " 1000" << ": " << numFaults1000 << std::endl;
        std::cout << "MRU" << " 2000" << ": " << numFaults2000 << std::endl;
        std::cout << "MRU" << " 5000" << ": " << numFaults5000 << std::endl;
        std::cout << "MRU" << " 10000" << ": " << numFaults10000 << std::endl;
        std::cout << "MRU" << " 20000" << ": " << numFaults20000 << std::endl;
        numFaults = numFaults1 + numFaults2 + numFaults5 + numFaults10 + numFaults20 + numFaults50 + numFaults100 + numFaults200 + numFaults500 + numFaults1000 + numFaults2000 + numFaults5000 + numFaults10000 + numFaults20000;
    }
    else{
        RBTree sortedTree;
        unsigned int MRUval = 0;
        unsigned int pageFirst = 0;
        unsigned int pageSecond = 0;
        while (fgets(buffer, BUFFER_SIZE, stdin)!=NULL){
            instruction = strtok(buffer,delimiters);
            if ((instruction != NULL)&&!(strcmp(instruction, "I")&&strcmp(instruction, "S")&&strcmp(instruction, "L")&&strcmp(instruction, "M"))){ /* Not NULL, and valid instruction */
                address = strtok(NULL, delimiters);
                if (address != NULL){ /* Not NULL */
                    size = strtok(NULL, delimiters);
                    if(size != NULL){ /* Not NULL */
                        unsigned long int addressNum = strtol(address, NULL, 16);
                        pageFirst = addressNum>>PAGE_SIZE_BITS;
                        pageSecond = (addressNum+strtol(size, NULL, 10)-1)>>PAGE_SIZE_BITS;
                        numFaults += MRUinsert(pageFirst, numPages, sortedTree, MRUval); /* Check first page */
                        MRUval = pageFirst;
                        if (pageSecond!=pageFirst){/* if we need a second page */
                            numFaults += MRUinsert(pageSecond, numPages, sortedTree, MRUval);
                            MRUval = pageSecond;
                        }
                    }
                }
            }
            memset(buffer,0,BUFFER_SIZE);
        }
        std::cout << "MRU " << numPages << ": " << numFaults << std::endl;
    }
    return numFaults;
}

int main(int argc, char * argv[]){
    int opt;
    char* policyName;
    int numPages = 0;
    unsigned long int numFaults = 0;
    int isSweep = 0;
    const char    * short_opt = "p:n:";
    struct option   long_opt[] =
    {
        {"policy", required_argument, NULL, 'p'},
        {"num-pages", required_argument, NULL, 'n'},
        {"debug", no_argument, NULL, 'd'},
        {NULL,            0,                 NULL, 0  }
    };
    while ((opt = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1){
        switch (opt){
            case 0:
                break;
            case 'n':
                if(!strcmp(optarg, "sweep")){
                    isSweep = 1;
                }
                else{
                    numPages = atoi(optarg);
                }
                break;
            case 'p':
                policyName = (char*)malloc(sizeof(optarg)+1);
                strcpy(policyName,optarg);
                break;
            case 'd':
                isDebug = 1;
                break;
            default:
                break;
        }
    }
    if (!strcmp(policyName, "random")){
        randomReplacement(isSweep, numPages);
    }
    else if (!strcmp(policyName, "FIFO")){
        FIFOreplacement(isSweep, numPages);
    }
    else if (!strcmp(policyName, "MRU")){
        MRUreplacement(isSweep, numPages);
    }
    else{
        std::cout<<"Unknown Policy"<<std::endl;
        return 0;
    }
    
    return 0;
}
