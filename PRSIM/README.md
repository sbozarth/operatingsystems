# PRISM

Page Replacement Simulator made by Daniel Bozarth. Reads in data from stdin. 

# Contents

FIXME

# Compile

Make Page Replacement Simulator:

`make prism`

or

`make`

Clean Directory:

`make clean`


# Run

`./prism --num-pages <number of pages> --policy <policy>`

## Supported Policies

FIFO and random are supported.

## Supported Page Numbers

1 to 20,000 pages are supported.

# Notes

