/************************************/
/* Authored by Daniel Scott Bozarth */
/************************************/
#define VERSION_NUM 0.2
#include "rbt.hpp"

Node *Node::uncle() { 
    if (parent == NULL || parent->parent == NULL) 
        return NULL; 

    if (parent->isOnLeft()) 
        return parent->parent->right; 
    else
        return parent->parent->left; 
} 
    
bool Node::isOnLeft() { return this == parent->left; } 
    
Node *Node::sibling() { 
    if (parent == NULL) 
        return NULL; 
    
    if (isOnLeft()) 
        return parent->right; 
    return parent->left; 
} 
    
void Node::moveDown(Node *nParent) { 
    if (parent != NULL) { 
        if (isOnLeft()) { 
            parent->left = nParent; 
        } 
        else { 
            parent->right = nParent; 
        } 
    } 
    nParent->parent = parent; 
    parent = nParent; 
} 
    
bool Node::hasRedChild() { 
    return (left != NULL && left->color == RED) || (right != NULL && right->color == RED); 
} 
  
void RBTree::leftRotate(Node *x) { 
    Node *nParent = x->right; 

    if (x == root) 
        root = nParent; 
    x->moveDown(nParent); 
    x->right = nParent->left; 
    if (nParent->left != NULL) 
        nParent->left->parent = x; 
    nParent->left = x; 
} 
 
void RBTree::rightRotate(Node *x) { 
    Node *nParent = x->left; 

    if (x == root) 
        root = nParent; 

    x->moveDown(nParent); 

    x->left = nParent->right; 
    if (nParent->right != NULL) 
        nParent->right->parent = x; 
    nParent->right = x; 
} 
  
void RBTree::swapColors(Node *x1, Node *x2) { 
    COLOR temp; 
    temp = x1->color; 
    x1->color = x2->color; 
    x2->color = temp; 
} 
  
void RBTree::swapValues(Node *u, Node *v) { 
    unsigned int temp; 
    temp = u->val; 
    u->val = v->val; 
    v->val = temp; 
} 
  
void RBTree::fixRedRed(Node *x) { 
    if (x == root) { 
        x->color = BLACK; 
        return; 
    } 

    /* initialize parent, grandparent, uncle */
    Node *parent = x->parent, *grandparent = parent->parent, 
        *uncle = x->uncle(); 

    if (parent->color != BLACK) { 
        if (uncle != NULL && uncle->color == RED) { /* uncle red */
            parent->color = BLACK; 
            uncle->color = BLACK; 
            grandparent->color = RED; 
            fixRedRed(grandparent); 
        } 
        else { 
            if (parent->isOnLeft()) { 
                if (x->isOnLeft()) { /* left right */
                    swapColors(parent, grandparent); 
                } 
                else { /* left left */
                    leftRotate(parent); 
                    swapColors(x, grandparent); 
                } 
                rightRotate(grandparent); 
            } 
            else { 
                if (x->isOnLeft()) { /* right left */
                    rightRotate(parent); 
                    swapColors(x, grandparent); 
                } 
                else { /* right right */
                    swapColors(parent, grandparent); 
                } 
                leftRotate(grandparent); 
            } 
        } 
    } 
} 

Node *RBTree::successor(Node *x) { /* find node without left child */
    Node *temp = x; 

    while (temp->left != NULL) 
    temp = temp->left; 

    return temp; 
} 

Node *RBTree::BSTreplace(Node *x) { /* find node that replaces deleted node */
    if (x->left != NULL && x->right != NULL) /* two child */
        return successor(x->right); 

    if (x->left == NULL && x->right == NULL) /* leaf */
        return NULL; 

    if (x->left != NULL) /* one child */
        return x->left; 
    else
        return x->right; 
}

void RBTree::removeNode(Node *v){
    Node *u = BSTreplace(v); 
    bool uvBlack = ((u == NULL || u->color == BLACK) && (v->color == BLACK)); 
    Node *parent = v->parent; 

    if (u == NULL) { /* if v is leaf */
        if (v == root) { 
            root = NULL; 
        } 
        else { 
            if (uvBlack) { /* v leaf, double black */
                fixDoubleBlack(v); 
            } 
            else { /* Red */
                if (v->sibling() != NULL) /* sibling exists */
                    v->sibling()->color = RED;
            } 
            if (v->isOnLeft()) { 
                parent->left = NULL; 
            } 
            else { 
                parent->right = NULL; 
            } 
        } 
        delete v; 
        return; 
    } 

    if (v->left == NULL || v->right == NULL) { /* 1 child */
        if (v == root) { 
            v->val = u->val; 
            v->left = v->right = NULL; 
            delete u; 
        } 
        else { 
            if (v->isOnLeft()) { 
                parent->left = u; 
            } 
            else { 
                parent->right = u; 
            } 
            delete v; 
            u->parent = parent; 
            if (uvBlack) { /* double black */
                fixDoubleBlack(u); 
            } 
            else { 
                u->color = BLACK; 
            } 
        } 
        return; 
    } 
    swapValues(u, v); 
    removeNode(u); 
} 

  
void RBTree::fixDoubleBlack(Node *x) { 
    if (x == root) 
        return; 

    Node *sibling = x->sibling(), *parent = x->parent; 
    if (sibling == NULL) { 
        fixDoubleBlack(parent); 
    } 
    else { 
        if (sibling->color == RED) { /* red sibling */ 
            parent->color = RED; 
            sibling->color = BLACK; 
            if (sibling->isOnLeft()) { /* left */
                rightRotate(parent); 
            } 
            else { /* right */
                leftRotate(parent); 
            } 
            fixDoubleBlack(x); 
        } 
        else { /* black sibling */
            if (sibling->hasRedChild()) { /* red child */
                if (sibling->left != NULL && sibling->left->color == RED) { 
                    if (sibling->isOnLeft()) { /* left left */
                        sibling->left->color = sibling->color; 
                        sibling->color = parent->color; 
                        rightRotate(parent); 
                    } 
                    else { /* right left */
                        sibling->left->color = parent->color; 
                        rightRotate(sibling); 
                        leftRotate(parent); 
                    } 
                } 
                else { 
                    if (sibling->isOnLeft()) { /* left right */
                        sibling->right->color = parent->color; 
                        leftRotate(sibling); 
                        rightRotate(parent); 
                    } 
                    else { /* right right */
                        sibling->right->color = sibling->color; 
                        sibling->color = parent->color; 
                        leftRotate(parent); 
                    } 
                } 
                parent->color = BLACK; 
            } 
            else { /* two black */
                sibling->color = RED; 
                if (parent->color == BLACK) 
                    fixDoubleBlack(parent); 
                else
                    parent->color = BLACK; 
            } 
        } 
    } 
} 

void RBTree::levelOrder(Node *x) { 
    if (x == NULL) /* If node doesnt exist */
        return; 

    std::queue<Node *> q; /* for printing */
    Node *curr; 

    q.push(x); 

    while (!q.empty()) { 
        curr = q.front(); 
        q.pop(); 
        std::cout << curr->val << " "; 
        if (curr->left != NULL) 
            q.push(curr->left); 
        if (curr->right != NULL) 
            q.push(curr->right); 
    } 
} 

void RBTree::inorder(Node *x) { 
    if (x == NULL) 
        return; 
    inorder(x->left); 
    std::cout << x->val << " "; 
    inorder(x->right); 
} 
  
RBTree::RBTree() { root = NULL; }

int RBTree::getSize(){
    return size;
}

Node *RBTree::getRoot() { return root; } 
  
/* returns the node or previous node */
Node *RBTree::search(unsigned int n) { 
    Node *temp = root; 
    while (temp != NULL) { /* while we can search */
        if (n < temp->val) { /* if requested is smaller */
            if (temp->left == NULL) /* and left is empty */
                break; /* we can go no further */
            else /* and left exists */
                temp = temp->left; /* go to left */
        } 
        else if (n == temp->val)  /* we have found the value */
            break; /* we are there */         
        else { /* requested is larger */
            if (temp->right == NULL) /* and right is empty */
                break; /* we can go no further */
            else /* and right exists */
                temp = temp->right; /* go to right */
        } 
    } 
    return temp; 
}

/* removes random node */
void RBTree::removeRandom() { 
    Node *temp = root; 
    int foundTarget = 0;
    while (temp != NULL && !foundTarget) { /* while we can search */
        switch(rand()%3){
            case 0:
                if (temp->left == NULL) /* left is empty */
                    foundTarget = 1;
                else /* left exists */
                    temp = temp->left; /* go to left */
                break;
            case 1:
                if (temp->right == NULL) /* right is empty */
                    foundTarget = 1;
                else /* right exists */
                    temp = temp->right; /* go to right */
                break;
            case 2:
                foundTarget = 1;
                break;
        }
    } 
    deleteVal(temp->val); 
}

void RBTree::insertVal(unsigned int n) {
    ++size;
    Node *newNode = new Node(n); 
    if (root == NULL) { /* If tree is empty */
        newNode->color = BLACK; /* Insert at root */
        root = newNode; 
    } 
    else { 
        Node *temp = search(n); 
        if (temp->val == n) { /* If value alread exists */
            return; 
        } 
        newNode->parent = temp; 

        if (n < temp->val) 
            temp->left = newNode; 
        else
            temp->right = newNode; 
        fixRedRed(newNode); 
    } 
} 

void RBTree::deleteVal(unsigned int n) { 
    if (root == NULL) /* If tree doesnt exist */
        return; 
    Node *v = search(n), *u; 
    if (v->val != n) { /* if not found */
        return; 
    }
    deleteNode(v); 
} 

void RBTree::deleteNode(Node *v){
    --size;
    removeNode(v);
}

void RBTree::printInOrder() { 
    std::cout << "Inorder: " << std::endl; 
    if (root == NULL) 
        std::cout << "Tree is empty" << std::endl; 
    else
        inorder(root); 
    std::cout << std::endl; 
} 

void RBTree::printLevelOrder() { 
    std::cout << "Level order: " << std::endl; 
    if (root == NULL) 
        std::cout << "Tree is empty" << std::endl; 
    else
        levelOrder(root); 
    std::cout << std::endl; 
}  
